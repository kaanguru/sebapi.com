/*
 *
 * HomePage
 *
 */
/* eslint-disable */
import React, { memo } from 'react';
import { FormattedMessage } from 'react-intl';
import { get, upperFirst } from 'lodash';
import { auth } from 'strapi-helper-plugin';
import PageTitle from '../../components/PageTitle';

import useFetch from './hooks';
import { ALink, Block, Container, LinkWrapper, P, Wave, Separator } from './components';


const FIRST_BLOCK_LINKS = [
  {
    link:
      'https://strapi.io/documentation/3.0.0-beta.x/getting-started/quick-start.html#_4-create-a-new-content-type',
    contentId: 'app.components.BlockLink.documentation.content',
    titleId: 'app.components.BlockLink.documentation',
  },
  {
    link: 'https://github.com/strapi/foodadvisor',
    contentId: 'app.components.BlockLink.code.content',
    titleId: 'app.components.BlockLink.code',
  },
];

const SOCIAL_LINKS = [
  {
    name: 'GitHub',
    link: 'https://github.com/strapi/strapi/',
  },
  {
    name: 'Slack',
    link: 'https://slack.strapi.io/',
  },
  {
    name: 'Medium',
    link: 'https://medium.com/@strapi',
  },
  {
    name: 'Twitter',
    link: 'https://twitter.com/strapijs',
  },
  {
    name: 'Reddit',
    link: 'https://www.reddit.com/r/Strapi/',
  },
  {
    name: 'Stack Overflow',
    link: 'https://stackoverflow.com/questions/tagged/strapi',
  },
];

const HomePage = ({ global: { plugins }, history: { push } }) => {
  const { error, isLoading, posts } = useFetch();
  const handleClick = e => {
    e.preventDefault();

    push(
      '/plugins/content-type-builder/content-types/plugins::users-permissions.user?modalType=contentType&kind=collectionType&actionType=create&settingType=base&forTarget=contentType&headerId=content-type-builder.modalForm.contentType.header-create&header_icon_isCustom_1=false&header_icon_name_1=contentType&header_label_1=null'
    );
  };
  const hasAlreadyCreatedContentTypes =
    get(plugins, ['content-manager', 'leftMenuSections', '0', 'links'], []).filter(
      contentType => contentType.isDisplayed === true
    ).length > 1;

  const headerId = hasAlreadyCreatedContentTypes
    ? 'HomePage.greetings'
    : 'app.components.HomePage.welcome';
  const username = get(auth.getUserInfo(), 'username', '');
  const linkProps = hasAlreadyCreatedContentTypes
    ? {
      id: 'app.components.HomePage.button.blog',
      href: 'https://blog.strapi.io/',
      onClick: () => { },
      type: 'blog',
      target: '_blank',
    }
    : {
      id: 'app.components.HomePage.create',
      href: '',
      onClick: handleClick,
      type: 'documentation',
    };

  return (
    <>
      <FormattedMessage id="HomePage.helmet.title">
        {title => <PageTitle title={title} />}
      </FormattedMessage>
      <Container className="container-fluid">
        <div className="row">
          <div className="col-lg-8 col-md-12">
            <Block>
              <Wave />
              <FormattedMessage
                id={headerId}
                values={{
                  name: upperFirst(username),
                }}
              >
                {msg => <h2 id="mainHeader">{msg}</h2>}
              </FormattedMessage>
              {hasAlreadyCreatedContentTypes ? (
                <FormattedMessage id="app.components.HomePage.welcomeBlock.content.again">
                  {msg => <P>Lüften bu kısımda dikkatli olunuz. Yedeklere geri dönmek zorunda kalınır.</P>}
                </FormattedMessage>
              ) : (
                  <FormattedMessage id="app.components.HomePage.welcomeBlock.content.again">
                    {msg => <P>Lüften bu kısımda dikkatli olunuz. Yedeklere geri dönmek zorunda kalınır.</P>}
                  </FormattedMessage>
                )}
              {hasAlreadyCreatedContentTypes && (
                <div style={{ marginTop: isLoading ? 60 : 50 }}>
                  Tüm gerekli düzetlmeler ve Ayarlar buradan yapılır.
                  <p> Sadece Kullanıcılar ve Birds  üzerinde değişiklik yapmak yeterli olacaktır.</p>
                  <h2>Terimler</h2>
                  <p>Collection Type --&gt; Kolleksiyon Türleri </p>
                  <p>Birds --&gt; Kuşlar </p>
                  <p>Diseases --&gt; Hastalıklar Tablosu</p>
                  <p>Eggs --&gt; Yumurtalar Tablosu</p>
                  <p>Races --&gt; Yarışmalar Listesi</p>
                  <p>Transfers --&gt; Kuş sahibi tarafından transfere açılmış Transfer kodlarının bulunduğu tablo </p>
                  <p>Vaccines --&gt; Aşılar tablosu </p>
                </div>
              )}

              <Separator style={{ marginTop: 37, marginBottom: 36 }} />
              <div style={{ display: 'flex', justifyContent: 'space-between' }}>

              </div>
            </Block>
          </div>

          <div className="col-md-12 col-lg-4">
            <Block style={{ paddingRight: 30, paddingBottom: 0 }}>
              <FormattedMessage id="HomePage.community">{msg => <h2>Destek</h2>}</FormattedMessage>
              <FormattedMessage id="app.components.HomePage.community.content">
                {content => <P style={{ marginTop: 7, marginBottom: 0 }}>Cem Kaan Kösalı - 555 490 8144 - www.cemkaan.com </P>}
              </FormattedMessage>


              <Separator style={{ marginTop: 18 }} />
              <div
                className="row social-wrapper"
                style={{
                  display: 'flex',
                  margin: 0,
                  marginTop: 36,
                  marginLeft: -15,
                }}
              >
                {/* {SOCIAL_LINKS.map((value, key) => (
                  <SocialLink key={key} {...value} />
                ))} */}
              </div>
            </Block>
          </div>
        </div>
      </Container>
    </>
  );
};

export default memo(HomePage);

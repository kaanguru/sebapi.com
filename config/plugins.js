module.exports = ({ env }) => ({
    // ...
    email: {
        provider: 'mailgun',
        providerOptions: {
            apiKey: env('MAILGUN_API_KEY'),
            domain: env('MAILGUN_DOMAIN'),
            host: env('MAILGUN_HOST'),
        },
        settings: {
            defaultFrom: 'yonetim@sebapi.com',
            defaultReplyTo: 'cem@cemkaan.com',
        },
    },
    graphql: {
        endpoint: '/graphql',
        shadowCRUD: true,
        playgroundAlways: false,
        depthLimit: 7,
        amountLimit: 300,
        apolloServer: {
            tracing: false,
        }
    }

    // ...
});
module.exports = ({ env }) => ({
  host: env('HOST', '0.0.0.0'),
  port: env.int('PORT', 1515),
  admin: {
    url: '/yonetim',
    auth: {
      secret: env('ADMIN_JWT_SECRET'),
    },
  },
});

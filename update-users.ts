/* 
async function updateUsers() {
    const filteredUsers = await strapi.query("user", "users-permissions").find({ Uyelik_Yenileme_lt: "2022-12-25", blocked: false, _sort: 'id:desc' })
    for (const theuser of filteredUsers) {
        await strapi.query("user", "users-permissions").update({ id: theuser.id }, { Uyelik_Yenileme: "2023-12-26" });
    }
} 
*/
// yarn strapi console
let beforeDate = "2021-12-29";
let extendDate = "2021-12-26";
async function updateUsers() { const filteredUsers = await strapi.query("user", "users-permissions").find({ Uyelik_Yenileme_lt: beforeDate, blocked: false, _sort: 'id:desc' }); for (const theuser of filteredUsers) { await strapi.query("user", "users-permissions").update({ id: theuser.id }, { Uyelik_Yenileme: extendDate }); } };
updateUsers();